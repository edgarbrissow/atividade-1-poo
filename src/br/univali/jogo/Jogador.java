package br.univali.jogo;

import java.util.ArrayList;

public class Jogador {
	private String nome;
	private int compras;
	private ArrayList<Peca> mao = new ArrayList<>();
	
	public int getCompras() {
		return compras;
	}
	public void setCompras(int compras) {
		this.compras = compras;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void delPeca(Peca peca) {
		this.mao.remove(peca);
	}
	public void addPeca(Peca peca) {
		this.mao.add(peca);
	}
	
	public void mostraMao(){
		System.out.println("\nMao do jogador");
		for(int i=0; i< mao.size();i++){
			System.out.println(mao.get(i).getLadoA() + "," + mao.get(i).getLadoB());
		}	
		
	}
	
	//To do: Arrumar esse procedimento;
	public void jogarPeca(Mesa mesa){
		for(int i=0; i < mao.size(); i++){
			if((mao.get(i).getLadoA() == mesa.getLados().getLadoA())){
				mao.get(i).inverteLados();
				mesa.addPeca(mao.get(i));
				mesa.organizaMesa();
				mao.remove(i);
			}else if((mao.get(i).getLadoA() == mesa.getLados().getLadoB())){
				mesa.addPeca(mao.get(i));
				mao.remove(i);
			}else if((mao.get(i).getLadoB() == mesa.getLados().getLadoA())){
				mesa.addPeca(mao.get(i));
				mesa.organizaMesa();
				mao.remove(i);
			}else if((mao.get(i).getLadoB() == mesa.getLados().getLadoB())){
				mao.get(i).inverteLados();
				mesa.addPeca(mao.get(i));
				mao.remove(i);
			}
		}
	}
	
	

}
