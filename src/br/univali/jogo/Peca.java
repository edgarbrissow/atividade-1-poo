package br.univali.jogo;

public class Peca {
	private int ladoA;
	private int ladoB;
	
	public int getLadoA() {
		return ladoA;
	}
	public void setLadoA(int ladoA) {
		this.ladoA = ladoA;
	}
	public int getLadoB() {
		return ladoB;
	}
	public void setLadoB(int ladoB) {
		this.ladoB = ladoB;
	}
	
	public void inverteLados(){
		ladoB = ladoA + ladoB;
		ladoA = ladoB - ladoA;
		ladoB = ladoB - ladoA;
	}
	

}
