package br.univali.jogo;

import java.util.ArrayList;

public class Mesa {
	private ArrayList<Peca> mesa = new ArrayList<>();
	private Peca lados = new Peca();
	public void limpaMesa() {
		mesa.clear();
	}
	public void addPeca(Peca peca) {
		mesa.add(peca);
	}
	
	public void organizaMesa(){
		Peca peca;
		peca = mesa.get(mesa.size()-1);
		for(int i= mesa.size() -1; i > 1 -1; i--){
			mesa.set(i, mesa.get(i-1));
		}
		mesa.set(0, peca);
	}

	public void mostraMesa(){
		System.out.println("\nMesa");
		for(int i=0; i< mesa.size();i++){
			System.out.printf("["+mesa.get(i).getLadoA() + "," + mesa.get(i).getLadoB()+"]");
		}
	}
	public Peca getLados(){		
		lados.setLadoA(mesa.get(0).getLadoA());
		lados.setLadoB(mesa.get(mesa.size()-1).getLadoB());
		return lados;
	}
	
	
}
