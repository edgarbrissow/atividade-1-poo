package br.univali.domino;

import java.util.ArrayList;

import br.univali.jogo.Jogador;
import br.univali.jogo.Mesa;
import br.univali.jogo.Peca;

public class Jogada {
	private static int numero;
	private int numeroJogadores;
	private ArrayList<Jogador> jogador = new ArrayList<>();
	private Mesa mesa;
	private ArrayList<Peca> pecas = new ArrayList<>();
	
	public void iniciar(){
		//To do: Perguntar a quantidade de jogadores.
		iniciaPecas();
		
		
	}
	
	private void iniciaPecas(){
		for(int i = 0; i < 7; i++){
			for(int j = i; j < 7; j++){
				Peca peca = new Peca();
				peca.setLadoA(i);
				peca.setLadoB(j);
				pecas.add(peca);
			}
		}
	}
	
	
	

}
