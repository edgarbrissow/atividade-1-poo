package br.univali.domino;



import br.univali.jogo.Jogador;
import br.univali.jogo.Mesa;
import br.univali.jogo.Peca;

public class Domino {
	
	public static void main(String args[]){
		
		Peca[] pecas = new Peca[28];
		int gambiarra =0;
		
		for(int i = 0; i < 7; i++){
			for(int j = i; j < 7; j++){
				pecas[gambiarra] = new Peca();
				pecas[gambiarra].setLadoA(i);
				pecas[gambiarra].setLadoB(j);
				gambiarra++;
			}
		}
		
		for(int i=0; i < gambiarra; i++ ){
			System.out.println(pecas[i].getLadoA() + "," + pecas[i].getLadoB() );
		}
		
		Jogador jogador = new Jogador();
		
		jogador.addPeca(pecas[5]);
		jogador.addPeca(pecas[23]);
		jogador.addPeca(pecas[19]);
		jogador.addPeca(pecas[26]);
		jogador.addPeca(pecas[6]);
		System.out.println("\nMao do jogador caro:");
		jogador.mostraMao();
		jogador.delPeca(pecas[5]);
		System.out.println("\nMao do jogador del:");
		jogador.mostraMao();
		Mesa mesa = new Mesa();
		mesa.addPeca(pecas[27]);
		mesa.mostraMesa();
		jogador.jogarPeca(mesa);
		mesa.mostraMesa();
		jogador.mostraMao();
		jogador.jogarPeca(mesa);
		mesa.mostraMesa();
		jogador.mostraMao();
		jogador.jogarPeca(mesa);
		mesa.mostraMesa();
		jogador.mostraMao();
		
		
		
		
		
	}


}
